const { checkLength, checkAlphabet, checkSymbol, checkDigit, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 8 characters to be true', () => {
    expect(checkLength('01234567')).toBe(true)
  })
})

describe('Test Alpahbet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })

  test('should has alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit 123456789 in password', () => {
    expect(checkDigit('123456789')).toBe(true)
  })

  test('should has digit 012345 in password', () => {
    expect(checkDigit('012345')).toBe(true)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password', () => {
    expect(checkSymbol('111!21')).toBe(true)
  })

  test('should has symbol # in password', () => {
    expect(checkSymbol('12#1')).toBe(true)
  })

  test('should has no symbol in password', () => {
    expect(checkSymbol('12145678')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password Boy@12 to be false', () => {
    expect(checkPassword('Boy@12')).toBe(false)
  })

  test('should has password Boy@12sa to be true', () => {
    expect(checkPassword('Boy@12sa')).toBe(true)
  })
})
